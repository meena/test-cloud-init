#!/bin/sh
set -e

# parse `-a arch` command line, and set default instance type
. ./aws-arch.sh

# create VPC and SecurityGroup
. ./aws-vpc.sh

# Create our Instance 
aws ec2 run-instances --image-id resolve:ssm:/aws/service/freebsd/${arch}/base/zfs/14.0/RELEASE \
	--instance-type ${instance_type} \
	--key-name aws \
	--associate-public-ip-address \
	--security-group-ids ${sec_gid}
