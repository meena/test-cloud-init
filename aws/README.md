# on AWS

create an AMI, this defaults to arm64:

```
./aws-create-ami.sh
```

copy files to new machine:

```
scp prepare-ami.sh cloud-98_ec2.cfg pkg-cloud-init.conf ec2-ip.region-1.compute.amazonaws.com:
```

Login to the machine and run `prepare-ami.sh`

```
local% ssh ec2-user@ec2-ip.region-1.compute.amazonaws.com
remote% su -
remote# cd /home/ec2-user
remote# ./prepare-ami.sh
```

create an AMI from the machine and note AMI ID in `aws-test-cloud-init.sh`.
Modify `user-data.yml` as needed and run `./aws-test-cloud-init.sh` to test the latest net/cloud-init-devel release.
