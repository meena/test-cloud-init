# Create a default VPC in case on doesn't exist
# Then get the VpcID, we'll need that.

vpc_id=$(aws ec2 create-default-vpc 2>/dev/null || aws ec2 describe-vpcs | jq --raw-output ".Vpcs[0].VpcId")
echo "'${vpc_id}'"

# Check if our SecurityGroup FreeBSD SSH already exists, and get the GroupID
sec_gid=$(aws ec2 describe-security-groups | jq --raw-output '.SecurityGroups[] | select(.GroupName == "FreeBSD SSH") | .GroupId')
if test -z "$sec_gid" ; then 
	# if not, create such that SecurityGroup, and get the GroupID
	sec_gid=$(aws ec2 create-security-group --group-name "FreeBSD SSH"  --description "FreeBSD allow ingress SSH access" --vpc-id $vpc_id | jq --raw-output ".GroupId" )
	aws ec2 authorize-security-group-ingress --group-id ${sec_gid} --protocol tcp --port 22 --cidr 0.0.0.0/0
fi
echo "'${sec_gid}'"
