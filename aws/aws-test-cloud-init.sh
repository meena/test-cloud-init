#!/bin/sh
set -e

# parse `-a arch` command line, and set default instance type
. ./aws-arch.sh

# create VPC and SecurityGroup
. ./aws-vpc.sh

if [ "$release" = "devel" -a "$arch" = "arm64" ] ; then
	  ami=ami-0fa0c14d210b9d184
elif [ "$release" = "release" -a "$arch" = "amd64" ] ; then
  ami=ami-099c3b658d9d7ff35
fi

# Create our Instance 
aws ec2 run-instances --image-id $ami \
	--instance-type ${instance_type} \
	--key-name aws \
	--associate-public-ip-address \
	--security-group-ids ${sec_gid} \
	--user-data file://user-data.yml
