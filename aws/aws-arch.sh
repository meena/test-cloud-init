arch=arm64
release=devel

usage(){
    echo "usage: $0 [-a amd64|arm64] [-r devel|release]"
}

args=$(getopt a:r: $*)
if [ $? -ne 0 ] ; then
	echo "usage ..."
	exit 2
fi
set -- $args
while :; do
	case "$1" in
		-a) arch="$2"
		    shift; shift;;
		-r) release="$2"
		shift; shift;;
		--) shift; break;;
	esac
done

case "$arch" in
    amd64) instance_type=t3.medium ;;
    arm64) instance_type=t4g.medium;;
    *) usage ;;
esac

case "$release" in
    devel) ;;
    release) ;;
    *) usage ;;
esac
