#!/bin/sh

release=devel
package="py39-cloud-init-devel"

usage() {
	  echo "usage: prepare-ami.sh [-r release|devel]"
	  exit 2
}

args=$(getopt r: $*)
if [ $? -ne 0 ] ; then
    usage
fi
set -- $args
while :; do
	  case "$1" in
		    -r) release="$2"
		        shift; shift;;
		    --) shift; break;;
	  esac
done

if [ "$release" = "devel" ] ; then
    package="py39-cloud-init-devel"
elif [ "$release" = "release" ] ; then
    package="py39-cloud-init"
else
    usage
fi

mkdir -p /usr/local/etc/cloud/cloud.cfg.d
mkdir -p /usr/local/etc/pkg/repos

install -o root -g wheel /home/ec2-user/cloud-98_ec2.cfg /usr/local/etc/cloud/cloud.cfg.d/98_ec2.cfg
install -o root -g wheel /home/ec2-user/pkg-cloud-init.conf /usr/local/etc/pkg/repos/cloud-init.conf

sort -u /etc/rc.conf > /tmp/rc
mv /tmp/rc /etc/rc.conf

sysrc ec2_configinit_enable="NO"
sysrc firstboot_freebsd_update_enable="NO"
sysrc firstboot_pkgs_list="${package}"
sysrc cloudinit_enable="YES"

bectl list -H | awk '$1 != "default" {print $1}' | xargs -n1 -x bectl destroy

pkg update
pkg clean -a

touch /firstboot
